#!/usr/bin/python
# -*- coding: utf-8 -*-

from lxml import html
import requests
from enum import Enum
import datetime
import gi
gi.require_version('Notify', '0.7')
from gi.repository import GLib, Notify
from zenipy import message
import subprocess
import argparse
import urllib


class Restaurant(Enum):
    R1 = 1
    R2 = 2
    R3 = 3


class Language(Enum):
    English = 1
    French = 2


class MenuItem:
    def __init__(self, name, t, price, d):
        self.name = name
        self.type = t
        self.price = price
        self.day = d

    def __str__(self):
        return "{}".format(self.name.encode('utf-8'))
        #return "<a href=\"https://www.google.com/search?q={0}\">{0}".format(self.type.encode('utf-8'), self.price.encode('utf-8'), self.name.encode('utf-8'))
        #return '<a href="http://www.google.com/search?q={0}">{1}</a>'.format(urllib.quote_plus(self.name.encode('utf-8')), self.name.encode('utf-8'))

class Menu:
    def __init__(self):
        self._items = []

    def add_item(self, item):
        self._items += [item]

    def get_by_day(self, d):
        return [i for i in self._items if i.day == d]
        
    def __str__(self):
        return "\n".join([str(i) for i in self._items])


class NovaeParser:
    URL = "http://www.novae-restauration.ch/menus/menu-week/cern/"
    restaurant = {Restaurant.R1: 13, Restaurant.R2: 21, Restaurant.R3: 33}
    language = {Language.English: 'en', Language.French: 'fr'}

    def __init__(self, lang, resto):
        self._url = "{}{}?lang={}".format(NovaeParser.URL, NovaeParser.restaurant[resto], NovaeParser.language[lang])
        self._menu = Menu()

    def parse(self):
        page = requests.get(self._url)
        tree = html.fromstring(page.content)
        dates = tree.xpath('//td[@class="EnteteMenu"]/text()')
        types = tree.xpath('//td[@class="typeMenu"]/text()')
        items = tree.xpath('//table[@class="HauteurMenu"]/tbody/tr/td/span/text()')
        prices_parent = tree.xpath('//table[@class="HauteurMenu"]/tbody/tr/td/center/table/tbody/tr/td')
        prices = [str(i.xpath('text()'))[2:-2] for i in prices_parent]
        days = {"Monday": 1, "Tuesday": 2, "Wednesday": 3, "Thursday": 4, "Friday": 5}
        for date in dates:
            day = date.split(" ")[0]
            for idx, t in enumerate(types):
                self._menu.add_item(MenuItem(items.pop(0), t, prices.pop(0), days[day]))
        return self._menu
    
    def get_url(self):
        return self._url


class App:
    def __init__(self):
        self._menu = []
        self._title = []
        self._text = []
        self._url = []
        self._args = self.parse_cmd_args()
        day = datetime.datetime.today().weekday()+1
        if day > 4:
            day = 4
        if self._args.tomorrow:
            if day >=44: day = 0
            else: day += 1
        for i, r in enumerate([Restaurant.R1, Restaurant.R2, Restaurant.R3]):
            parser = NovaeParser(Language.English, r)
            menu = parser.parse()
            self._url += [parser.get_url()]
            self._menu += [menu]
            self._title += ["Restaurant {}".format(i+1)]
            self._text += [" • ".join(map(str, menu.get_by_day(day)))]

        self._display = 0
        
        Notify.init("Menu scraper")
        self._notification = Notify.Notification.new(
            self._title[self._display],
            self._text[self._display],
            'dialog-information'
        )

        #self._notification.set_timeout(Notify.EXPIRES_NEVER)
        self._notification.set_timeout(600000)
        self._notification.connect('closed', self.stop)

        self._notification.add_action(
            "toggle",
            "Switch restaurants",
            self.toggle_menu,
            self._notification
        )
        
        self._notification.add_action(
            "open_url",
            "Open full menu",
            self.open_menu,
            self._notification
        )

        self.loop = GLib.MainLoop()
            
    def parse_cmd_args(self):
        desc = 'Display notifications with CERN restaurants menus.'
        parser = argparse.ArgumentParser(description=desc)
        parser.add_argument('-r', '--restaurant', type=int, help='Which restaurant to check (1, 2 or 3)', default=1)
        parser.add_argument('-t', '--tomorrow', help='Log file path (default: log_scenario)', action='store_true')
        return parser.parse_args()

    def run(self):
        self._notification.show()
        self.loop.run()

    def stop(self, a):
        self.loop.quit()

    def toggle_menu(self, a, b, n):
        self._display += 1
        if self._display > 2:
            self._display = 0
        self._notification.update(self._title[self._display], self._text[self._display], 'dialog-information')
        self._notification.show()
        
    def open_menu(self, a, b, n):
        subprocess.Popen(["xdg-open", self._url[self._display]])
        self._notification.show()


if __name__ == "__main__":
    App().run()

